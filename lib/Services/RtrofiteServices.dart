import 'dart:convert';

import 'package:api_demo/Models/ComentModels.dart';
import 'package:http/http.dart' as http;

import '../Models/User_model.dart';
class RetrofiteServices{

  Future<List<UsersModel>?> getUserDetails()async{
    var uri = "https://jsonplaceholder.typicode.com/posts";

    try{
      var response = await http.get(Uri.parse(uri));
      if(response.statusCode == 200){
        List jsonData = jsonDecode(response.body);
        return jsonData.map((e) => UsersModel.fromJson(e)).toList();
      }
      else{
       // server error
      }
    }catch(stackEception){
    return Future.error("failuar");
    }
  }
  
  Future<List<ComentModel>?> comentDetails()async{
    var comentUri = Uri.parse("https://jsonplaceholder.typicode.com/comments");
    try{
      var comentResponse = await http.get(comentUri);
      if(comentResponse.statusCode == 200){
        List comentData = jsonDecode(comentResponse.body);
        return comentData.map((e) => ComentModel.fromJson(e)).toList();
      }
    }catch(comentExeption){
      return Future.error("coment section error");

    }
  }
}