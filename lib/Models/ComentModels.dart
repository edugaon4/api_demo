// To parse this JSON data, do
//
//     final comentModel = comentModelFromJson(jsonString);

import 'dart:convert';

List<ComentModel> comentModelFromJson(String str) => List<ComentModel>.from(json.decode(str).map((x) => ComentModel.fromJson(x)));

String comentModelToJson(List<ComentModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ComentModel {
  ComentModel({
  required  this.postId,
  required  this.id,
  required  this.name,
  required this.email,
  required  this.body,
  });

  int postId;
  int id;
  String name;
  String email;
  String body;

  factory ComentModel.fromJson(Map<String, dynamic> json) => ComentModel(
    postId: json["postId"],
    id: json["id"],
    name: json["name"],
    email: json["email"],
    body: json["body"],
  );

  Map<String, dynamic> toJson() => {
    "postId": postId,
    "id": id,
    "name": name,
    "email": email,
    "body": body,
  };
}
