import 'package:api_demo/Models/ComentModels.dart';
import 'package:flutter/material.dart';
import '../Services/RtrofiteServices.dart';
import 'Third_page.dart';

class SecondPage extends StatefulWidget {
  const SecondPage({Key? key}) : super(key: key);

  @override
  State<SecondPage> createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Coments"),
      ),
      body: FutureBuilder(
        future: RetrofiteServices().comentDetails(),
        builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot){
          if(snapshot.hasData){
            List<ComentModel> usersComent = snapshot.data;
            return ListView.builder(itemCount: usersComent.length,itemBuilder: (context, index){
              return Padding(
                padding: const EdgeInsets.all(4.0),
                child: InkWell(
                  onTap:(){
                    Navigator.push(context,MaterialPageRoute(builder: (context)=> ThirdPage()));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(10)
                    ),
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       mainAxisAlignment: MainAxisAlignment.start,
                       children: [
                         Text(usersComent[index].id.toString(), style: const TextStyle(color: Colors.white),),
                         Text(usersComent[index].postId.toString(), style: const TextStyle(color: Colors.white),),
                         Text(usersComent[index].email, style: const TextStyle(color: Colors.white),),
                         Text(usersComent[index].name, style: const TextStyle(color: Colors.white),),
                         Text(usersComent[index].body, style: const TextStyle(color: Colors.white),),
                       ],
                     ),
                   )
                  ),
                ),
              );
            });
          }
          else{
            return const Center(child: CircularProgressIndicator());
          }


        },
      ),
    );
  }
}
