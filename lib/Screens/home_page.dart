import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../Models/User_model.dart';
import '../Services/RtrofiteServices.dart';
import 'Second_page.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}
class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("UserDemo"),
      ),
      body: FutureBuilder(
        future: RetrofiteServices().getUserDetails(),
        builder:(BuildContext context, AsyncSnapshot<dynamic> snapshot){
          if(snapshot.hasData){
            List<UsersModel> data = snapshot.data;
            return ListView.builder(itemCount: data.length,itemBuilder:(context, index){
             return Padding(
               padding: const EdgeInsets.all(4.0),
               child: InkWell(
                 onTap:(){
                   Navigator.push(context, MaterialPageRoute(builder: (context)=> SecondPage()));
                 },
                 child: Container(
                   decoration: BoxDecoration(
                     color: Colors.grey,
                       borderRadius: BorderRadius.circular(10),
                   ),
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         Text(data[index].id.toString(), style: const TextStyle(color: Colors.white, fontSize: 15),),
                         Text(data[index].userId.toString(), style: const TextStyle(color: Colors.white, fontSize: 15),),
                         Text(data[index].title, style: const TextStyle(color: Colors.white, fontSize: 15),),
                         Text(data[index].body, style: const TextStyle(color: Colors.white, fontSize: 15),),

                       ],
                     ),
                   )
                 ),
               ),
             );
            });
          }
          else{
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Increment',
        onPressed: () {  },
        child: const Icon(Icons.add),
      ),
    );
  }
}
